/* Copyright (C) 2020 Tobias Leupold <tobias.leupold@gmx.de>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3 or any later version
   accepted by the membership of KDE e. V. (or its successor approved
   by the membership of KDE e. V.), which shall act as a proxy
   defined in Section 14 of version 3 of the license.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

// Local includes
#include "ImagesList.h"
#include "ImageCache.h"
#include "ImageItem.h"
#include "Settings.h"

// KDE includes
#include <KLocalizedString>

// Qt includes
#include <QIcon>
#include <QDebug>
#include <QApplication>
#include <QMouseEvent>
#include <QDrag>
#include <QMimeData>
#include <QFileInfo>
#include <QMenu>
#include <QAction>

// C++ includes
#include <functional>

ImagesList::ImagesList(ImagesList::Type type, Settings *settings, ImageCache *imageCache,
                       QWidget *parent)
    : QListWidget(parent),
      m_type(type),
      m_settings(settings),
      m_imageCache(imageCache)
{
    setSortingEnabled(true);
    setIconSize(m_imageCache->thumbnailSize());

    connect(this, &QListWidget::currentItemChanged, this, &ImagesList::imageHighlighted);
    connect(this, &QListWidget::itemClicked,
            this, std::bind(&ImagesList::imageHighlighted, this, std::placeholders::_1, nullptr));

    m_contextMenu = new QMenu(this);

    if (m_type == Type::Assigned) {
        m_lookupElevation = m_contextMenu->addAction(i18n("Lookup elevation"));
        connect(m_lookupElevation, &QAction::triggered, [this]
                { emit lookupElevation(dynamic_cast<ImageItem *>(currentItem())->path()); });
    }

    m_removeCoordinates = m_contextMenu->addAction(i18n("Remove coordinates"));
    connect(m_removeCoordinates, &QAction::triggered, [this]
            { emit removeCoordinates(dynamic_cast<ImageItem *>(currentItem())->path()); });

    m_discardChanges = m_contextMenu->addAction(i18n("Discard changes"));
    connect(m_discardChanges, &QAction::triggered, [this]
            { emit discardChanges(dynamic_cast<ImageItem *>(currentItem())->path()); });

    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, &QListWidget::customContextMenuRequested, this, &ImagesList::showContextMenu);
}

void ImagesList::imageHighlighted(QListWidgetItem *item, QListWidgetItem *) const
{
    if (item != nullptr) {
        emit imageSelected(dynamic_cast<ImageItem *>(item)->path());
    }
}

void ImagesList::addOrUpdateImage(const QString &path)
{
    bool itemFound = false;
    ImageItem *imageItem;

    const QFileInfo info(path);
    const QString fileName = info.fileName();

    // Check for an existing entry we want to update
    for (int i = 0; i < count(); i++) {
        if (dynamic_cast<ImageItem *>(item(i))->path() == path) {
            imageItem = dynamic_cast<ImageItem *>(item(i));
            itemFound = true;
            break;
        }
    }

    if (! itemFound) {
        // We need a new item
        imageItem = new ImageItem(QIcon(QPixmap::fromImage(m_imageCache->thumbnail(path))),
                                  fileName, path);
    }

    imageItem->setChanged(m_imageCache->changed(path));
    imageItem->setMatchType(m_imageCache->matchType(path));

    if (! itemFound) {
        addItem(imageItem);
    }

    if (currentItem() == imageItem) {
        // Update the preview without centering the image
        emit imageSelected(path, false);
    }
}

QVector<QString> ImagesList::allImages() const
{
    QVector<QString> paths;
    for (int i = 0; i < count(); i++) {
        paths.append(dynamic_cast<ImageItem *>(item(i))->path());
    }
    return paths;
}

void ImagesList::removeImage(const QString &path)
{
    for (int i = 0; i < count(); i++) {
        if (dynamic_cast<ImageItem *>(item(i))->path() == path) {
            const auto *item = takeItem(i);
            delete item;
            return;
        }
    }
}

void ImagesList::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        m_dragStartPosition = event->pos();
    }

    QListWidget::mousePressEvent(event);
}

void ImagesList::mouseMoveEvent(QMouseEvent *event)
{
    const auto *item = currentItem();

    if (! (event->buttons() & Qt::LeftButton)
        || item == nullptr
        || (event->pos() - m_dragStartPosition).manhattanLength()
           < QApplication::startDragDistance()) {

        return;
    }

    auto *drag = new QDrag(this);
    drag->setPixmap(item->icon().pixmap(iconSize()));

    QMimeData *mimeData = new QMimeData;
    mimeData->setText(dynamic_cast<const ImageItem *>(item)->path());
    drag->setMimeData(mimeData);
    drag->exec(Qt::MoveAction);
}

void ImagesList::showContextMenu(const QPoint &point)
{
    const auto *item = currentItem();
    if (item == nullptr) {
        return;
    }

    if (m_lookupElevation != nullptr) {
        m_lookupElevation->setEnabled(m_settings->lookupElevation());
    }

    const QString &path = dynamic_cast<const ImageItem *>(item)->path();
    m_removeCoordinates->setEnabled(m_imageCache->coordinates(path).isSet);
    m_discardChanges->setEnabled(m_imageCache->changed(path));

    m_contextMenu->exec(mapToGlobal(point));
}
